Todo list application
=====================

Zadanie 1
---------
Utwórz REST API będące back-endem dla aplikacji web/mobile będącej listą zadań do wykoanania (todo list)
Funkcjonalność API jest następująca:
  - pobranie listy zadań
  - pobranie konkretnego zadania
  - dodanie nowego zadania
  - edycja istniejącego zadania
  - usunięcie zadania

Listę zadań zapisz w pamięci. Każde zadanie posiada:
  - treść zadania
  - termin wykonania zadania
  - priorytet
  - flaga oznaczajaca wykonanie zadania
  - opcjonalnie: możliwość ustawienia kolejności zadań na wyświetlanej liście
​
W implementacji użyj wzorca Model-View-Controller opartego o Spring MVC.

Zadanie 2
---------
Dodaj warstwę utrwalania w bazie danych opartą o Spring Data JPA

Zadanie 3
---------
Zmodyfikuj metodę kontolera zwracającą listę zadań o możliwość sortowania wg priorytetu oraz filtrowania wg treści zadania

Zadanie 4
---------
Dodaj interejs użytkownika w postaci aplikacji webowej

Zadanie 5
---------
Dodaj obsługę wielu języków (np. angielski/niemiecki/polski). Wyświetlany język powinien być uzależniony od preferencji danego użytkownika

Zadanie 6
---------
Dodaj obsługę wielu użytkowników w aplikacji. Dane użytkowników powinny być zapisane w bazie danych. Każdy z użytkowników powinien posiadać własne konto w aplikacji i własną listę zadań.