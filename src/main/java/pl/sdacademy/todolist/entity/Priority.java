package pl.sdacademy.todolist.entity;

public enum Priority {
    HIGH, NORMAL, LOW;
}