package pl.sdacademy.todolist.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.sdacademy.todolist.dto.TaskDto;
import pl.sdacademy.todolist.entity.User;
import pl.sdacademy.todolist.service.TaskService;

import javax.validation.Valid;
import java.util.List;

@RequiredArgsConstructor
@Controller
@RequestMapping({"/", "/tasks"})
public class TaskController {

    private final TaskService taskService;

    @GetMapping
    public String taskList(Model model, @AuthenticationPrincipal User user) {
        List<TaskDto> tasks = taskService.findAllByUsername(user.getUsername());
        model.addAttribute("taskList", tasks);
        model.addAttribute("taskForm", new TaskDto());
        return "tasks";
    }

    @GetMapping("/{id}")
    public String taskFrom(@PathVariable Long id,
                           @RequestParam(required = false, name = "a") String action,
                           Model model, @AuthenticationPrincipal User user) {
        if ("delete".equals(action)) {
            taskService.delete(id, user.getUsername());
            return "redirect:/tasks";
        }

        TaskDto task = taskService.find(id, user.getUsername());
        model.addAttribute("taskForm", task);
        return "taskForm";
    }

    @PostMapping
    public String addTask(@Valid TaskDto taskForm, @AuthenticationPrincipal User user) {
        taskService.create(taskForm, user.getUsername());
        return "redirect:/tasks";
    }

    @PostMapping("/edit")
    public String editTask(@Valid @ModelAttribute TaskDto taskForm, @AuthenticationPrincipal User user) {
        taskService.update(taskForm, user.getUsername());
        return "redirect:/tasks";
    }
}
