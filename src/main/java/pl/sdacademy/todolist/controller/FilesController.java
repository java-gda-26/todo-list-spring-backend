package pl.sdacademy.todolist.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import pl.sdacademy.todolist.entity.User;
import pl.sdacademy.todolist.service.FileService;

@RequiredArgsConstructor
@Controller
@RequestMapping("/files")
public class FilesController {

    private final FileService fileService;

    @GetMapping
    public Resource userPhoto(@AuthenticationPrincipal User user) {
        return fileService.findUserPhoto(user.getUsername());
    }

    @PostMapping("/photo")
    public String uploadUserPhoto(@RequestParam("photo") MultipartFile file, @AuthenticationPrincipal User user) {
        fileService.save(user.getUsername(), file);
        return "redirect:/account";
    }
}
