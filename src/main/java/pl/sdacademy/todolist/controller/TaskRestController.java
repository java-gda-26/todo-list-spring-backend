package pl.sdacademy.todolist.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import pl.sdacademy.todolist.dto.ErrorDto;
import pl.sdacademy.todolist.dto.PageDto;
import pl.sdacademy.todolist.dto.TaskDto;
import pl.sdacademy.todolist.entity.Task;
import pl.sdacademy.todolist.exception.EntityNotFoundException;
import pl.sdacademy.todolist.service.TaskService;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/tasks")
public class TaskRestController {

    private final TaskService taskService;

    @GetMapping
    public List<Task> findAll(@Valid PageDto page) {
        log.info("Fetching list from database");
        return taskService.findAll(page);
    }

    @GetMapping("/{id}")
    public TaskDto findTask(@PathVariable Long id, Principal principal) {
        return taskService.find(id, principal.getName());
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public TaskDto addTask(@Valid @RequestBody TaskDto task, Principal principal) {
        return taskService.create(task, principal.getName());
    }

    @PutMapping("/{id}")
    public TaskDto task(@Valid @RequestBody TaskDto task, @PathVariable Long id, Principal principal) {
        task.setId(id);
        return taskService.update(task, principal.getName());
    }

    @DeleteMapping("/{id}")
    public void deleteTask(@PathVariable Long id, Principal principal) {
        taskService.delete(id, principal.getName());
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler({EntityNotFoundException.class})
    public ErrorDto handleNotFoundException(EntityNotFoundException ex) {
        return handleException(ex);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({BindException.class, ConstraintViolationException.class, MethodArgumentNotValidException.class})
    public ErrorDto handleValidationException(Exception ex) {
        return handleException(ex);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler({Exception.class})
    public ErrorDto handleGeneralException(Exception ex) {
        return handleException(ex);
    }

    private ErrorDto handleException(Exception ex) {
        log.error("Exception handled", ex);
        ErrorDto error = new ErrorDto();
        error.setExceptionClass(ex.getClass().getCanonicalName());
        error.setMessage(ex.getMessage());
        return error;
    }
}