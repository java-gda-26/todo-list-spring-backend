package pl.sdacademy.todolist.dto;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class PageDto {

    private String filter;

    @Min(0)
    @NotNull
    private Integer pageIndex;

    @Min(1)
    @Max(20)
    @NotNull
    private Integer pageSize;

    @NotNull
    private String sortDirection;

    @NotNull
    private String sortColumn;
}
