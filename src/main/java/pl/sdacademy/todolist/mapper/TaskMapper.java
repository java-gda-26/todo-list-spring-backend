package pl.sdacademy.todolist.mapper;

import org.mapstruct.Mapper;
import pl.sdacademy.todolist.dto.TaskDto;
import pl.sdacademy.todolist.entity.Task;

@Mapper(componentModel = "spring")
public interface TaskMapper {

    Task map(TaskDto dto);

    TaskDto map(Task entity);
}
