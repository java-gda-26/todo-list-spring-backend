package pl.sdacademy.todolist.mapper;

import org.mapstruct.Mapper;
import pl.sdacademy.todolist.dto.UserDto;
import pl.sdacademy.todolist.entity.User;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserDto map(User user);
}
