package pl.sdacademy.todolist.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import pl.sdacademy.todolist.dto.UserDto;
import pl.sdacademy.todolist.entity.User;
import pl.sdacademy.todolist.exception.EntityNotFoundException;
import pl.sdacademy.todolist.mapper.UserMapper;
import pl.sdacademy.todolist.repository.UserRepository;

import javax.transaction.Transactional;

@RequiredArgsConstructor
@Service
public class UserService {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final UserMapper userMapper;

    public void create(UserDto userDto) {
        User entity = new User();
        entity.setFirstName(userDto.getFirstName());
        entity.setUsername(userDto.getUsername());

        String encodedPassword = passwordEncoder.encode(userDto.getPassword());
        entity.setPassword(encodedPassword);
        userRepository.save(entity);
    }

    public UserDto findByUsername(String username) {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new EntityNotFoundException("User not found"));
        return userMapper.map(user);
    }

    @Transactional
    public void update(String username, UserDto userDto) {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new EntityNotFoundException("User not found"));
        user.setFirstName(userDto.getFirstName());
        if (!StringUtils.isEmpty(userDto.getPassword())) {
            String encodedPassword = passwordEncoder.encode(userDto.getPassword());
            user.setPassword(encodedPassword);
        }
    }
}
