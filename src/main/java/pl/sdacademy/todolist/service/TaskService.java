package pl.sdacademy.todolist.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import pl.sdacademy.todolist.dto.PageDto;
import pl.sdacademy.todolist.dto.TaskDto;
import pl.sdacademy.todolist.entity.Priority;
import pl.sdacademy.todolist.entity.Task;
import pl.sdacademy.todolist.entity.User;
import pl.sdacademy.todolist.exception.EntityNotFoundException;
import pl.sdacademy.todolist.mapper.TaskMapper;
import pl.sdacademy.todolist.repository.TaskRepository;
import pl.sdacademy.todolist.repository.UserRepository;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class TaskService {

    private final TaskMapper taskMapper;
    private final TaskRepository taskRepository;
    private final UserRepository userRepository;

    public List<TaskDto> findAllByUsername(String username) {
        return taskRepository.findAllByUserUsername(username).stream()
                .map(taskMapper::map)
                .collect(Collectors.toList());
    }

    public List<Task> findAll(PageDto pageDto) {
        Sort.Direction sortDirection = Sort.Direction.fromString(pageDto.getSortDirection());
        Sort sort = Sort.by(sortDirection, pageDto.getSortColumn());
        Pageable page = PageRequest.of(pageDto.getPageIndex(), pageDto.getPageSize(), sort);
        if (StringUtils.isEmpty(pageDto.getFilter())) {
            return taskRepository.findAll(page).getContent();
        }
        return taskRepository.findAllByDescriptionLike("%" + pageDto.getFilter() + "%", page);
    }

    public TaskDto find(Long id, String username) {
        return taskRepository.findByIdAndUserUsername(id, username)
                .map(taskMapper::map)
                .orElseThrow(() -> new EntityNotFoundException(id));
    }

    public TaskDto create(TaskDto dto, String username) {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new EntityNotFoundException("User with username " + username + " not found"));
        Task entity = taskMapper.map(dto);
        entity.setUser(user);
        Task savedEntity = taskRepository.save(entity);
        return taskMapper.map(savedEntity);
    }

    public TaskDto update(TaskDto dto, String username) {
        Task updatedTask = taskRepository.findByIdAndUserUsername(dto.getId(), username)
                .orElseThrow(() -> new EntityNotFoundException(dto.getId()));
        updatedTask.setDescription(dto.getDescription());
        updatedTask.setPriority(Priority.valueOf(dto.getPriority()));
        taskRepository.save(updatedTask);
        return taskMapper.map(updatedTask);
    }

    public void delete(Long id, String username) {
        Task task = taskRepository.findByIdAndUserUsername(id, username)
                .orElseThrow(() -> new EntityNotFoundException(id));
        taskRepository.delete(task);
    }
}
