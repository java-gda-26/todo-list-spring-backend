package pl.sdacademy.todolist.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import pl.sdacademy.todolist.entity.Task;

import java.util.List;
import java.util.Optional;

public interface TaskRepository extends JpaRepository<Task, Long> {

//    @Query("select t from Task t where t.user.username = :user")
//    List<Task> findAllByUserName(@Param("user") String username);

//    List<Task> findAllByUser(User user);

    List<Task> findAllByUserUsername(String username);

    List<Task> findAllByDescriptionLike(String filter, Pageable page);

    Optional<Task> findByIdAndUserUsername(Long id, String username);
}