package pl.sdacademy.todolist.controller;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import pl.sdacademy.todolist.entity.Priority;
import pl.sdacademy.todolist.entity.Task;
import pl.sdacademy.todolist.entity.User;
import pl.sdacademy.todolist.repository.TaskRepository;
import pl.sdacademy.todolist.repository.UserRepository;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@WithMockUser(username = "testUser")
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integration-test.properties")
@ExtendWith(SpringExtension.class)
public class TaskRestControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private UserRepository userRepository;

    private User testUser;

    @BeforeEach
    public void setUp() {
        User user = new User();
        user.setUsername("testUser");
        user.setPassword("p4ssw0rd");
        user.setFirstName("Jan");
        userRepository.save(user);
        testUser = user;
    }

    @AfterEach
    public void tearDown() {
        taskRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    public void getAllTest() throws Exception {
        // given
        String task1Description = "task1";
        String task2Description = "task2";
        Task task1 = create(task1Description);
        Task task2 = create(task2Description);
        taskRepository.save(task1);
        taskRepository.save(task2);

        // when
        mockMvc.perform(
                    get("/api/tasks?pageIndex=0&pageSize=10&sortDirection=ASC&sortColumn=id")
                    .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].description", is(task1Description)))
                .andExpect(jsonPath("$[1].description", is(task2Description)));
    }

    @Test
    public void getOneTest() throws Exception {
        // given
        String description = "description";
        Task task = create(description);
        Long id = taskRepository.save(task).getId();

        // when
        mockMvc.perform(get("/api/tasks/{id}", id).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(Integer.valueOf(String.valueOf(id)))))
                .andExpect(jsonPath("$.description", is(description)));
    }

    @Test
    public void createTest() throws Exception {
        // given
        String taskDescription = "task description";
        String taskPriority = "HIGH";
        String taskJson = "{\"description\":\"{description}\",\"priority\":\"{priority}\"}"
                .replace("{description}", taskDescription)
                .replace("{priority}", taskPriority);

        // when
        mockMvc.perform(post("/api/tasks").contentType(MediaType.APPLICATION_JSON).content(taskJson))
                .andExpect(status().isCreated());

        // then
        List<Task> tasks = taskRepository.findAll();
        assertThat(tasks).hasSize(1);

        Task result = tasks.get(0);
        assertThat(result.getDescription()).isEqualTo(taskDescription);
        assertThat(result.getPriority()).isEqualTo(Priority.valueOf(taskPriority));
    }

    @Test
    public void createNotValidTest() throws Exception {
        // given
        String taskDescription = "";
        String taskJson = "{\"description\":\"{description}\"}".replace("{description}", taskDescription);

        // when
        mockMvc.perform(post("/api/tasks").contentType(MediaType.APPLICATION_JSON).content(taskJson))
                .andExpect(status().isBadRequest());

        // then
        List<Task> result = taskRepository.findAll();
        assertThat(result).isEmpty();
    }

    @Test
    public void updateTest() throws Exception {
        // given
        Task task = new Task();
        task.setDescription("some description");
        task.setPriority(Priority.LOW);
        task.setUser(testUser);

        Long taskId = taskRepository.save(task).getId();

        String taskDescription = "new description";
        String taskPriority = "HIGH";
        String taskJson = "{\"id\":{id},\"description\":\"{description}\",\"priority\":\"{priority}\"}"
                .replace("{id}", String.valueOf(taskId))
                .replace("{description}", taskDescription)
                .replace("{priority}", taskPriority);

        // when
        mockMvc.perform(put("/api/tasks/{id}", taskId).contentType(MediaType.APPLICATION_JSON).content(taskJson))
                .andExpect(status().isOk());

        // then
        Task result = taskRepository.findById(taskId).orElseThrow(IllegalArgumentException::new);
        assertThat(result.getDescription()).isEqualTo(taskDescription);
        assertThat(result.getPriority()).isEqualTo(Priority.HIGH);
    }

    @Test
    public void updateNotValidTest() throws Exception {
        // given
        long taskId = 1L;
        String taskDescription = "";
        String taskJson = "{\"id\":{id},\"description\":\"{description}\"}"
                .replace("{id}", String.valueOf(taskId))
                .replace("{description}", taskDescription);

        // when
        mockMvc.perform(put("/api/tasks/{id}", taskId).contentType(MediaType.APPLICATION_JSON).content(taskJson))
                .andExpect(status().isBadRequest());

        // then
        List<Task> result = taskRepository.findAll();
        assertThat(result).isEmpty();
    }

    @Test
    public void deleteTest() throws Exception {
        // given
        String description = "task description";
        Task task = create(description);
        Long taskId = taskRepository.save(task).getId();

        // when
        mockMvc.perform(delete("/api/tasks/{id}", taskId))
                .andExpect(status().isOk());

        // then
        List<Task> tasks = taskRepository.findAll();
        assertThat(tasks).isEmpty();
    }

    private Task create(String description) {
        Task entity = new Task();
        entity.setDescription(description);
        entity.setPriority(Priority.NORMAL);
        entity.setUser(testUser);
        return entity;
    }
}