FROM ubuntu:18.04

RUN apt update
RUN apt -y install openjdk-8-jdk

ADD target/todolist.jar /opt/todolist.jar

ENTRYPOINT ["/usr/bin/java", "-jar", "/opt/todolist.jar"]